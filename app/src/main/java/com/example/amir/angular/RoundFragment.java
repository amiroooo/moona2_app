package com.example.amir.angular;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.dd.CircularProgressButton;
import com.github.library.bubbleview.BubbleTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Amir on 12/31/2016.
 */

public class RoundFragment extends Fragment {
    private static String PARCELABLE_ITEM = "parcel_item";
    private RoundItem roundItem;
    private View root;

    private CircularProgressButton startButton;

    private Button saveObstacleButton;
    private TextView cronMainTextView;
    private AutofitTextView groupName;

    private ElegantNumberButton leftTop;

    private RelativeLayout relativeLayout;

    private CardView pointsCardView;
    private CardView obstacleCard;
    private CardView editRoundCard;


    private Toast toastObject;
    private BubbleTextView bubbleTextVew;


    private RecyclerView recyclerView;
    private ArrayList<RoundPartItem> adapterData;
    private RecyclerRoundPartAdapter roundItemAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetach() {
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onDetach();
    }

    public String getRoundItemId(){
        if (roundItem==null){
            return "";
        }
        return roundItem.get_id();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemDeletion(ItemToDelete event){
        if ((event.getDocumentId()).equals(roundItem.get_id())){
            FragmentManager frmgr = getFragmentManager();
            if (frmgr != null) {

                if (((RecyclerRoundPartAdapter)(recyclerView.getAdapter())).mainCountdownTimer1 != null){
                    ((RecyclerRoundPartAdapter)(recyclerView.getAdapter())).mainCountdownTimer1.cancel();
                    ((RecyclerRoundPartAdapter)(recyclerView.getAdapter())).mainCountdownTimer1 = null;
                }
                frmgr.popBackStack();
            }
        }
    }




    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRoundPartChangeData(RoundPartDataTypeChange event){
        if (adapterData != null){
            for (int i = 0; i < adapterData.size(); i++) {
                if (adapterData.get(i).get_id().equals(event.get_id())){
                    recyclerView.getRecycledViewPool().clear();
                    adapterData.set(i, (RoundPartItem) Config.roundPartsMap.get(event.get_id()));
                    if (event.isDataRequireRefresh()) {
                        roundItemAdapter.notifyItemChanged(i);
                    }
                }
            }
        }
    }





    private void showTalkbackDurationTooltip (String data) {
        if ( bubbleTextVew != null && bubbleTextVew.getVisibility () != View.VISIBLE ) {
            bubbleTextVew.setText (data);
            bubbleTextVew.setVisibility ( View.VISIBLE );
            AnimHelper.scaleInOut ( bubbleTextVew, 300, 0.15f );
            bubbleTextVew.postDelayed ( new Runnable () {
                @Override
                public void run ( ) {
                    bubbleTextVew.setVisibility ( View.GONE );
                }
            }, 1000 );
        }
    }


    private void bindRoundData() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            roundItem = bundle.getParcelable(PARCELABLE_ITEM);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.round_fragment2, container, false);
        initViews(root);
        bindData();
        initListeners();
        return root;
    }



    private void initViews(View v) {

        relativeLayout = (RelativeLayout)v.findViewById(R.id.frag_relative);
        recyclerView = (RecyclerView)v.findViewById(R.id.round_item_recycler);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private ArrayAdapter<String> getAdapterForSpinner(ArrayList<RecyclerItem> recyclerItems,String obstacle, int type){
        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("none");
        for(RecyclerItem recyclerItem : recyclerItems){
            String testString = recyclerItem.getOfficialName();
            if (testString != null){
                if (obstacle.equals(testString)){
                    stringList.add(0,testString);
                }else
                stringList.add(testString);
            }
        }

        ArrayAdapter<String> spinnerArrayAdapter;
        if (type ==1){
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.red_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.red_spinner_dropdown);
        }else if (type ==2){
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.blue_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.blue_spinner_dropdown);
        }else {
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, stringList);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }
        return spinnerArrayAdapter;
    }

    private ArrayAdapter<String> getAdapterForSpinnerNoNull(ArrayList<RecyclerItem> recyclerItems,String selected,int type){
        ArrayList<String> stringList = new ArrayList<>();
        if (selected.equals("none")){
            stringList.add(0,selected);
        }
        for(RecyclerItem recyclerItem : recyclerItems){
            String testString = recyclerItem.getOfficialName();
            if (testString != null){
                if (selected.equals(recyclerItem.getOfficialName())){
                    stringList.add(0,recyclerItem.getOfficialName());
                }else
                    stringList.add(recyclerItem.getOfficialName());
            }
        }
        ArrayAdapter<String> spinnerArrayAdapter;
        if (type ==1){
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.red_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.red_spinner_dropdown);
        }else if (type ==2){
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.blue_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.blue_spinner_dropdown);
        }else {
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, stringList);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }
        return spinnerArrayAdapter;
    }

    private ArrayAdapter<String> getAdapterForSpinnerNoNull(ArrayList<RecyclerItem> recyclerItems,long selected){
        ArrayList<String> stringList = new ArrayList<>();
        for(RecyclerItem recyclerItem : recyclerItems){
            if (recyclerItem instanceof  RoundItem){
                RoundItem round =  (RoundItem)recyclerItem;
                /*
                if (round.getRoundTime() == selected){
                    stringList.add(0,Long.toString(round.getRoundTime()));
                }else
                    stringList.add(Long.toString(round.getRoundTime()));
                */
            }
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, stringList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        return spinnerArrayAdapter;
    }

    private void bindData(){
        if (roundItem != null) {
            HashMap<String,RoundPartItem> roundParts = getRoundParts(roundItem.get_id());
            adapterData = new ArrayList<>();
            switch (roundItem.getRound_type()){
                case 0:{
                    RoundPartItem cronoItem = roundParts.get("main_cronometer");
                    if (cronoItem != null){
                        adapterData.add(cronoItem);
                    }

                    RoundPartItem phase1Item = roundParts.get("phase1");
                    if (phase1Item != null){
                        adapterData.add(phase1Item);
                    }
                    RoundPartItem phase2Item = roundParts.get("phase2");
                    if (phase2Item != null){
                        adapterData.add(phase2Item);
                    }
                    RoundPartItem phase3Item = roundParts.get("phase3");
                    if (phase3Item != null){
                        adapterData.add(phase3Item);
                    }
                    RoundPartItem copterItem = roundParts.get("copter");
                    if (copterItem != null){
                        adapterData.add(copterItem);
                    }
                    RoundPartItem catcherItem = roundParts.get("catcher");
                    if (catcherItem != null){
                        adapterData.add(catcherItem);
                    }
                    RoundPartItem innovationItem = roundParts.get("innovation");
                    if (innovationItem != null){
                        adapterData.add(innovationItem);
                    }


                    break;
                }
                case 1:{

                    RoundPartItem cronoItem = roundParts.get("main_cronometer");
                    if (cronoItem != null){
                        adapterData.add(cronoItem);
                    }
                    RoundPartItem comunityScoreItem = roundParts.get("community_score");
                    if (comunityScoreItem != null){
                        adapterData.add(comunityScoreItem);
                    }
                    break;
                }
            }

            roundItemAdapter = new RecyclerRoundPartAdapter(roundItem.get_id());
            roundItemAdapter.setData(adapterData);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(roundItemAdapter);
            recyclerView.setItemViewCacheSize(20);
        }
    }



    private HashMap<String,RoundPartItem> getRoundParts(String roundId){
        HashMap<String,RoundPartItem> result =  new HashMap<>();
        JSONObject value = null;
        try {
            value = new JSONObject().put("main_timer",roundItem.getRound_time()).put("data_type","main_timer").put("progressed","1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        result.put("main_cronometer",new RoundPartItem("0",roundItem.get_id(),"main_cronometer", value ));
        Iterator<RecyclerItem> iterator = Config.roundPartsMap.values().iterator();
        while(iterator.hasNext()) {
            RoundPartItem roundPartItem = (RoundPartItem)iterator.next();
            if (roundPartItem.getRound_id().equals(roundId)){
                result.put(roundPartItem.getRound_part_type(),roundPartItem);
            }
        }
        return result;
    }




    private void initListeners(){

    }

    private void handleRequestFinishMatchRound() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Do you want the match to move to review?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestReviewRound();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder1.create();
        alert.show();
    }

    private void requestReviewRound() {
        JSONArray params = new JSONArray();
        params.put(roundItem.get_id());
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.REVIEW_ROUND,params,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                startButton.setProgress(-1);
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "review_round");
    }

    private void handleRequestResetRound() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Do you want to reset this round?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestResetRound();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder1.create();
        alert.show();
    }


    public void requestResetRound(){
        JSONArray params = new JSONArray();
        Log.d("RoundFragment", "requestResetRound: id:  "+ roundItem.get_id());
        params.put(roundItem.get_id());
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.RESET_ROUND,params,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                onBackPressed();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                startButton.setProgress(-1);
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "reset_round");
    }




    private void handleRequestApproveRound() {

        JSONArray params = new JSONArray();
        params.put(roundItem.get_id());
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.APPROVE_ROUND,params,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //roundItem.setIsDone(4);
                startButton.setCompleteText("Complete");
                startButton.setIdleText("Complete");
                startButton.setProgress(0);
                startButton.setProgress(100);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                startButton.setProgress(-1);
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "approve_round");



    }

    private void handleRequestEditRound() {
        JSONArray params = new JSONArray();
        params.put(roundItem.get_id());

        //params.put(date.getTime());

        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND,params,new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                    onBackPressed();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "update_round_obstacles");



    }






    @Override
    public void onPause() {
        /*
        if (cronometer != null) {
            cronometer.setText("");
            //cronometer.setTextColor(ContextCompat.getColor(getContext(), R.color.colorRed));
        }
        */
        /*
        if (countDownTimer2 != null){
            countDownTimer2.cancel();
        }

        if (countDownTimer1 != null){
            countDownTimer1.cancel();
        }
        */

        super.onPause();
    }



    private void handleRequestStartRound() {

        JSONArray params = new JSONArray();

        params.put(roundItem.get_id());


        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.START_ROUND,params,new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                /*
                if(countDownTimer2 != null) {
                    countDownTimer2.cancel();
                }
                startButton.setProgress(-1);
                */
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "start_round");
    }


    private void handleRequestUpdateGroupPoints(final int group,final int obs,final int sign) {
        // && checkLimits(group,obs,sign)


            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Updating Points...");
            pDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.SERVER_URL + Config.ADD_POINTS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.equals("success")){
                        showTalkbackDurationTooltip ("Points added successfully.");
                    }else if (response.equals("failed")){
                        showTalkbackDurationTooltip ("This obstacle points at Maximum.");
                    }else if (response.equals("stateDone")){
                        showTalkbackDurationTooltip ("The round is already approved.");
                    }else{
                        showTalkbackDurationTooltip ("Round didn't start yet.");
                        if (Config.AdminStatus >0 && Config.AdminStatus<3){
                            if (!response.equals(roundItem.get_id())){
                                FindRoundByIdAndShowPerma findRoundByIdAndShowPerma = new FindRoundByIdAndShowPerma(response);
                                EventBus.getDefault().post(findRoundByIdAndShowPerma);
                            }
                        }
                    }

                    pDialog.cancel();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                    Context con = getContext();
                    if (con != null){
                        if (toastObject != null) {
                            toastObject.cancel();
                        }
                        toastObject = Toast.makeText(con,"Adding Points Failed!",Toast.LENGTH_LONG);
                        toastObject.show();
                    }
                    con = null;
                    pDialog.cancel();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    if (group == 1) {

                        params.put("groupName", roundItem.getGroup());

                    }
                    params.put("roundId", roundItem.get_id());
                    return params;
                }
            };
            MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "add_points");

    }

    private void handleRequestDeleteRound() {

    }


    private void handleRequestSaveRound() {


        JSONArray params = new JSONArray();

        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.ADD_NEW_ROUND,params,new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "add_new_round");

    }

    void onBackPressed(){

        if (roundItem.getRound_type() == 0 && Config.isUserFieldReferee() && !isRoundDone()){
            return;
        }
        if (roundItem.getRound_type() == 1 && Config.isUserCommunityReferee() && !isRoundDone()){
            return;
        }
        FragmentManager frmgr = getFragmentManager();
        if (frmgr != null) {

            if (((RecyclerRoundPartAdapter)(recyclerView.getAdapter())).mainCountdownTimer1 != null){
                ((RecyclerRoundPartAdapter)(recyclerView.getAdapter())).mainCountdownTimer1.cancel();
                ((RecyclerRoundPartAdapter)(recyclerView.getAdapter())).mainCountdownTimer1 = null;
            }
            frmgr.popBackStack();
        }
    }

    public boolean isRoundDone(){
        ArrayList<RoundPartItem> roundPartItems = roundItemAdapter.getData();
        boolean isAllDone = true;
        for (int i = 0; i < roundPartItems.size(); i++) {
            RoundPartItem roundPartItem = roundPartItems.get(i);
            if (!roundPartItem.isProgressed()){
                isAllDone = false;
                break;
            }
        }
        return isAllDone;
    }
}
