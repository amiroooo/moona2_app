package com.example.amir.angular;

/**
 * Created by Amir on 3/30/2018.
 */

class OnMoveToTab {
    int tab;

    public OnMoveToTab(int tab) {
        this.tab = tab;
    }

    public int getTab() {
        return tab;
    }
}
