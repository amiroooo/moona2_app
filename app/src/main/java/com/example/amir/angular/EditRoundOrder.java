package com.example.amir.angular;

/**
 * Created by Amir on 1/19/2017.
 */
public class EditRoundOrder {
    public RoundOrder getRoundOrder() {
        return roundOrder;
    }

    private RoundOrder roundOrder;

    public EditRoundOrder(RoundOrder id) {
        this.roundOrder = id;
    }
}
