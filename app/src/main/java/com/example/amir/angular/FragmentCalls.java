package com.example.amir.angular;

import android.support.annotation.Nullable;

/**
 * Created by Amir on 12/30/2016.
 */
public interface FragmentCalls {
    void initiateRefresh(int id);
    void onCardViewClick(int adapterId,RecyclerItem recyclerItem);
    void onCardViewLongClick(int adapterId,@Nullable String itemIdentifier);
}
