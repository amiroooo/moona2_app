package com.example.amir.angular;

/**
 * Created by Amir on 12/31/2016.
 */

public abstract class RecyclerItem implements Comparable<RecyclerItem>{
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    private String _id;
    private boolean listing = false;

    @Override
    public int compareTo(RecyclerItem o) {
        if (o instanceof GroupItem){
            return toString().compareTo(o.toString());
        }
        if (o instanceof RoundItem){
            if (((RoundItem)this).getRound_order() < ((RoundItem)o).getRound_order()){
                return -1;
            }
            if (((RoundItem)this).getRound_order() > ((RoundItem)o).getRound_order()){
                return 1;
            }
        }
        return 0;
    }
    abstract String getOfficialName();

    public boolean isListing() {
        return listing;
    }
    public void setListing( boolean lising){
        this.listing = lising;
    }
}
