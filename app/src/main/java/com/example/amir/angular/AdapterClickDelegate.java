package com.example.amir.angular;

import android.support.annotation.Nullable;

/**
 * Created by Amir on 12/31/2016.
 */
public interface AdapterClickDelegate {
    void onCardViewClick(int adapterId,RecyclerItem recyclerItem);
    void onCardViewLongClick(int type, String objectId);
}
