package com.example.amir.angular;

import org.json.JSONObject;

/**
 * Created by Amir on 1/18/2017.
 */
public class UpdateRoundDelegate {
    public String getDocumentID() {
        return documentID;
    }

    public JSONObject getUpdatedValuesJson() {
        return updatedValuesJson;
    }

    private String documentID;
    private JSONObject updatedValuesJson;

    public UpdateRoundDelegate(String documentID, JSONObject updatedValuesJson) {
        this.documentID = documentID;
        this.updatedValuesJson = updatedValuesJson;
    }
}
