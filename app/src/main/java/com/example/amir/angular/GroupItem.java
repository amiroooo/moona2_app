package com.example.amir.angular;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Amir on 12/31/2016.
 */

public class GroupItem extends RecyclerItem implements Parcelable {
    private String groupName;
    private String description;
    private String winnerType;
    private double points;
    private double adminExtra;
    private int number = -1;

    public GroupItem() {

    }

    public Integer getNumber() {

        return number;
    }

    public String getNumberAsString() {
        if (number == -1){
            return "";
        }
        return ((Integer)number).toString();
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public GroupItem(GroupItem groupItem) {
        this.groupName = groupItem.groupName;
        this.description = groupItem.description;
        this.winnerType = groupItem.winnerType;
        this.set_id(groupItem.get_id());
    }

    protected GroupItem(Parcel in) {
        groupName = in.readString();
        description = in.readString();
        winnerType = in.readString();
        points = in.readDouble();
        adminExtra = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(groupName);
        dest.writeString(description);
        dest.writeString(winnerType);
        dest.writeDouble(points);
        dest.writeDouble(adminExtra);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GroupItem> CREATOR = new Creator<GroupItem>() {
        @Override
        public GroupItem createFromParcel(Parcel in) {
            return new GroupItem(in);
        }

        @Override
        public GroupItem[] newArray(int size) {
            return new GroupItem[size];
        }
    };

    public String getGroupName() {
        return groupName;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @Override
    public String getOfficialName() {
        return getGroupName();
    }



    public String getWinnerType() {
        if (winnerType == null) return "";
        return winnerType;
    }

    public void setWinnerType(String winnerType) {
        this.winnerType = winnerType;
    }

    @Override
    public String toString() {
        return groupName;
    }

    public void updateByJson(JSONObject changedData) {
        try{
            setPoints(changedData.getDouble("points"));
        }catch(Throwable e){

        }
        try{
            if (changedData.has("admin_extra")) {
                setAdminExtra(changedData.getDouble("admin_extra"));
            }
        }catch(Throwable e){

        }
    }

    public void setAdminExtra(double adminExtra) {
        this.adminExtra = adminExtra;
    }

    public double getAdminExtra() {
        return adminExtra;
    }
}
