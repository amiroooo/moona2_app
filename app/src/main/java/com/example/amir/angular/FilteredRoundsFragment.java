package com.example.amir.angular;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.dd.CircularProgressButton;
import com.github.library.bubbleview.BubbleTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Amir on 12/31/2016.
 */

public class FilteredRoundsFragment extends Fragment implements AdapterClickDelegate{
    private static String PARCELABLE_ITEM = "parcel_item";
    private GroupItem groupItem;
    private View root;


    private Toast toastObject;

    private RecyclerView recyclerView;
    private ArrayList<RecyclerItem> adapterData;
    private RecyclerAdapter roundAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetach() {
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onDetach();
    }

    public String getGroupItemId(){
        if (groupItem==null){
            return "";
        }
        return groupItem.get_id();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemDeletion(ItemToDelete event){
        /*
        if ((event.getDocumentId()).equals(groupItem.get_id())){
            FragmentManager frmgr = getFragmentManager();
            if (frmgr != null) {
                frmgr.popBackStack();
            }
        }
        */
    }




    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRoundChangeData(UpdateRoundDelegate event){
        if (adapterData != null){
            for (int i = 0; i < adapterData.size(); i++) {
                if (adapterData.get(i).get_id().equals(event.getDocumentID())){
                    recyclerView.getRecycledViewPool().clear();
                    adapterData.set(i, (RoundItem) Config.roundsMap.get(event.getDocumentID()));
                    roundAdapter.notifyItemChanged(i);
                }
            }
        }
    }





    private void showTalkbackDurationTooltip (String data) {

    }


    private void bindRoundData() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            groupItem = bundle.getParcelable(PARCELABLE_ITEM);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.round_fragment2, container, false);
        initViews(root);
        bindData();
        initListeners();
        return root;
    }



    private void initViews(View v) {

        recyclerView = (RecyclerView)v.findViewById(R.id.round_item_recycler);
        TextView title = (TextView)v.findViewById(R.id.title);
        title.setText(getString(R.string.rounds_of) + groupItem.getGroupName());
        title.setVisibility(View.VISIBLE);

        FloatingActionButton filteredFab = (FloatingActionButton)v.findViewById(R.id.filtered_fab);
        if (!Config.isUserObserver()) {
            filteredFab.setVisibility(View.VISIBLE);
            filteredFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CreateNewRound t = new CreateNewRound(getActivity());
                    t.isSelected = getGroupIndexByName(groupItem.getGroupName());
                    t.show();
                }
            });
        }
    }

    private Integer getGroupIndexByName(String groupName) {
        ArrayList<RecyclerItem> arr = new ArrayList<>();
        arr.addAll(Config.groupsMap.values());
        Collections.sort(arr);
        for (int i = 0; i < arr.size(); i++) {
            GroupItem temp = (GroupItem)arr.get(i);
            if (temp.getGroupName().equals(groupName)){
                return i;
            }
        }
        return -1;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }



    private void bindData(){
        // build array with rounds of specific groupId
        if (groupItem != null) {
            setFilteredData();
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setItemViewCacheSize(20);
        }
    }

    public void setFilteredData(){
        if (groupItem != null) {
            HashMap<String, RecyclerItem> roundsPerGroup = getRoundPerGroup(groupItem.get_id());
            adapterData = new ArrayList<>();
            adapterData.addAll(roundsPerGroup.values());

            roundAdapter = new RecyclerAdapter(this, 3);
            roundAdapter.recognitionId = groupItem.get_id();
            roundAdapter.setDataByArrayList(adapterData);
            recyclerView.setAdapter(roundAdapter);
        }
    }

    private HashMap<String,RecyclerItem> getRoundPerGroup(String groupId){
        HashMap<String,RecyclerItem> result =  new HashMap<>();

        Iterator<RecyclerItem> iterator = Config.roundsMap.values().iterator();
        while(iterator.hasNext()) {
            RoundItem roundItem = (RoundItem)iterator.next();
            if (roundItem.getGroup().equals(groupId)){
                if (isRoundOkToReferee(roundItem.getRound_type())) {
                    result.put(roundItem.get_id(), roundItem);
                }
            }
        }
        return result;
    }

    private boolean isRoundOkToReferee(Integer round_type) {
        if (Config.isUserAdmin()){
            return true;
        }
        if (Config.isUserFieldReferee() && round_type == 0){
            return true;
        }
        if (Config.isUserCommunityReferee() && round_type == 1){
            return true;
        }
        return false;
    }


    private void initListeners(){

    }



    @Override
    public void onPause() {
        super.onPause();
    }


    void onBackPressed(){

        FragmentManager frmgr = getFragmentManager();
        if (frmgr != null) {
            frmgr.popBackStack();
        }
    }

    @Override
    public void onCardViewClick(int adapterId, RecyclerItem recyclerItem) {
        FragmentManager fmgr = getFragmentManager();
        RoundFragment roundFragment = new RoundFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("parcel_item",(Parcelable)recyclerItem);
        bundle.putBoolean("isPerma",false);
        roundFragment.setArguments(bundle);
        fmgr.beginTransaction().setCustomAnimations(R.anim.slide_from_right_to_left,R.anim.slide_to_exit_left,R.anim.slide_from_right_to_left,R.anim.slide_to_exit_left).addToBackStack("containterFragment").replace(R.id.fragment_container,roundFragment).commitAllowingStateLoss();
    }

    @Override
    public void onCardViewLongClick(int type, final String objectId) {
        if (!Config.isUserObserver()) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
            builder1.setMessage("Do you want to delete this round?");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            requestDeleteRound(objectId);
                            dialog.cancel();
                        }
                    });

            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }
    private void requestDeleteRound(String _id) {
        Log.d("FilteredFragment", "requestDeleteGroup: ");
        String tag_json_obj = "DeleteGroupRequest";
        String url = Config.SERVER_URL + Config.DELETE_ROUNDS + "/" + _id;;

        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading...");
        pDialog.show();
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("FilteredFragment", "onErrorResponse: "+error);
                VolleyLog.d("FilteredFragment", "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }
}
