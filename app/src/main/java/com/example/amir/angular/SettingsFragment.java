package com.example.amir.angular;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;


import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;


import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Amir on 12/31/2016.
 */

public class SettingsFragment extends Fragment {
    private EditText IPEditBox;
    private Button saveButton;
    private Button backButton;
    private TextView ipData;
    private Button remoteButton;
    private Button lastLocalButton;
    private SettingsDelegate settingsDelegate;
    private Spinner spinner;
    private CardView addpointsLayout;

    private Spinner groupToAddPointsTo;
    private EditText addPointsEditBox;
    private Button addPointsButton;
    private EditText passwordEditBox;

    private Button restoreDBButton;
    private Button backUpDBButton;

    private Button resetPointsButton;
    private Button resetSeasonButton;

    private Button switchRefreshButton;
    private Toast toastObject;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        settingsDelegate = (SettingsDelegate)context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        settingsDelegate = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.settings_fragment, container, false);
        initViews(root);
        bindData();
        initListeners();
        return root;
    }


    private void initViews(View v) {

        IPEditBox = (EditText)v.findViewById(R.id.editText);
        saveButton = (Button) v.findViewById(R.id.button_save);
        backButton = (Button)v.findViewById(R.id.back_button);
        ipData = (TextView) v.findViewById(R.id.ip_data);
        remoteButton = (Button) v.findViewById(R.id.remote_server);
        lastLocalButton = (Button) v.findViewById(R.id.last_local);
        spinner = (Spinner) v.findViewById(R.id.spinner);


        groupToAddPointsTo = (Spinner) v.findViewById(R.id.spinner5);
        addPointsEditBox = (EditText) v.findViewById(R.id.addpoints);


        passwordEditBox = (EditText) v.findViewById(R.id.pass_edit);


        addPointsButton = (Button) v.findViewById(R.id.add_points_button);

        addpointsLayout = (CardView)v.findViewById(R.id.round_duration1_layout);


        backUpDBButton = (Button) v.findViewById(R.id.save_db);

        resetSeasonButton = (Button) v.findViewById(R.id.reset_season);



    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private void bindData(){
        ipData.setText(Config.SERVER_URL);
        groupToAddPointsTo.setAdapter( getAdapterForSpinner(Config.groupsMap));
        spinner.setAdapter( getAdapterDefaultForSpinner(R.array.admin_status));

        spinner.setSelection(Config.AdminStatus);
        if (Config.AdminStatus > 0){
            addpointsLayout.setVisibility(View.GONE);
        }
    }

    private void initListeners(){
        saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleRequestSaveIp();
                    onBackPressed();
                }
        });


        addPointsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleRequestAddPoints();
            }
        });



        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        remoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IPEditBox.setText(Config.REMOTE_URL);
            }
        });
        lastLocalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("userdetails", MODE_PRIVATE);
                if (sharedPreferences.getString("localdetails","null").equals("null")){
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("localdetails",Config.LOCAL_URL);
                    edit.apply();
                    IPEditBox.setText(Config.LOCAL_URL);
                }else{
                    IPEditBox.setText(sharedPreferences.getString("localdetails","null"));
                }

            }
        });

        /*
        restoreDBButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpDBChange(true);
            }
        });
        */
        backUpDBButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpDBChange(false);
            }
        });


        /*
        resetPointsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPoints();
            }
        });
        */
        resetSeasonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetSeason();
            }
        });

        /*
        switchRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestSwitchAutoRefresh();
            }
        });
        */
    }

    private void resetPoints() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Are you sure you want to reset the points to 0 to all groups?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestResetPoints();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void requestResetPoints() {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Resetting Group points ...");
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.SERVER_URL + Config.RESET_GROUP_POINTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Amir", "Resetting Group points success");
                EventBus.getDefault().post(new SaveBackupOnDevice());
                pDialog.cancel();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Amir", "Resetting Group points failed");
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                pDialog.cancel();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "reset_group_points");
    }

    private void resetSeason() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Are you sure you want to reset the whole season?(all rounds will be deleted and all groups points to 0)");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestResetSeason();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    private void requestResetSeason() {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Resetting Season...");
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.SERVER_URL + Config.RESET_SEASON, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Amir", "Resetting Season success");
                EventBus.getDefault().post(new SaveBackupOnDevice());
                pDialog.cancel();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Amir", "Resetting Season failed");
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                pDialog.cancel();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "reset_season");
    }


    private void requestSwitchAutoRefresh() {
        EventBus.getDefault().post(new SwitchRefreshTables());
    }

    private void popUpDBChange(final boolean isRestore){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage(isRestore?"Are you sure you want to RESET Database?":"Are you sure you want to Backup the Database?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (isRestore) {
                            //requestDatabaseRestore();
                            requestDatabaseRestoreFromDevice();
                        }else
                            requestDatabaseBackup();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();


    }


    private void requestDatabaseBackup() {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Creating Database Backup ...");
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.SERVER_URL + Config.CREATE_BACKUP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Amir", "Creating backup success");
                EventBus.getDefault().post(new SaveBackupOnDevice());
                pDialog.cancel();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Amir", "Creating backup failed");
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                pDialog.cancel();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "create_backup");
    }

    private void requestDatabaseRestore() {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Restoring Database from Last Backup ...");
        pDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.SERVER_URL + Config.RESTORE_BACKUP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Amir", "Restoring Backup success");
                pDialog.cancel();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Amir", "Restoring Backup failed");
                VolleyLog.d("Amir", "Restoring backup response Error: " + error.getMessage());
                pDialog.cancel();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "restoring_backup");
    }


    //back_up_data_on_device

    private void requestDatabaseRestoreFromDevice() {
        JSONArray params = new JSONArray();
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("userdetails", MODE_PRIVATE);
        //Log.d("SettingsFragment", "static: "+MainActivity.dataBackedOnServer);
        Log.d("SettingsFragment", "saved: "+sharedPreferences.getString("back_up_data_on_device","null"));
        params.put(sharedPreferences.getString("back_up_data_on_device","null"));
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.RESTORE_FROM_DEVICE,params,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("SettingsFragment", "onResponse: successfuly restored from device saved data");

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "restore_database_from_device");
    }


    private void handleRequestAddPoints() {
        if (TextUtils.isEmpty(addPointsEditBox.getText())) return;
        JSONArray params = new JSONArray();

        RecyclerItem group = (RecyclerItem)Config.groupsMap.values().toArray()[groupToAddPointsTo.getSelectedItemPosition()];
        params.put(group.get_id());
        params.put(addPointsEditBox.getText());
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.ADD_RAW_POINTS,params,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                onBackPressed();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "add admin extra score");


    }


    private ArrayAdapter<RecyclerItem> getAdapterForSpinner(HashMap<String,RecyclerItem> recyclerItems){
        ArrayList<RecyclerItem> arr = new ArrayList<>();
        arr.addAll(recyclerItems.values());
        Collections.sort(arr);
        ArrayAdapter<RecyclerItem> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, arr );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown); // The drop down view
        return spinnerArrayAdapter;
    }


    private ArrayAdapter<String> getAdapterDefaultForSpinner(int id){
        String[] sarr = getResources().getStringArray(id);
        ArrayList<String> arr = new ArrayList<>(Arrays.asList(sarr));
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, arr );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown); // The drop down view
        return spinnerArrayAdapter;
    }




    private void handleRequestSaveIp() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("userdetails", MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        String k = IPEditBox.getText().toString();
        if (!TextUtils.isEmpty(k)){
            Config.SERVER_URL = k;
        }

        edit.putString("saved_ip_address",Config.SERVER_URL);
        if(!(IPEditBox.getText().toString()).equals(Config.REMOTE_URL) && !TextUtils.isEmpty(Config.SERVER_URL)){
            edit.putString("localdetails",IPEditBox.getText().toString());
        }

        if (validateCredentials() || spinner.getSelectedItemPosition() == 3){
            //Config.AdminStatus = spinner.getSelectedItemPosition();
            edit.putInt("admintype", spinner.getSelectedItemPosition());
        }

        if (spinner.getSelectedItemPosition() > 0 && spinner.getSelectedItemPosition() < 3 && validateRefereeCredentials()){
            //Config.AdminStatus = spinner.getSelectedItemPosition();
            edit.putInt("admintype", spinner.getSelectedItemPosition());
        }

        edit.apply();
        if (settingsDelegate != null){
            settingsDelegate.performReset();
        }

        getActivity().finish();
        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);

    }


    public boolean validateCredentials(){
        if (passwordEditBox.getText().toString().equals("adminmoona123")){
            return true;
        }
        return false;
    }


    public boolean validateRefereeCredentials(){
        if (passwordEditBox.getText().toString().equals("moona123")){
            return true;
        }
        return false;
    }




    @Override
    public void onPause() {
        super.onPause();
    }


    void onBackPressed(){
        getFragmentManager().popBackStack();
    }


}
