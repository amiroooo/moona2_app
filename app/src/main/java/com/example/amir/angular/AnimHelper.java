package com.example.amir.angular;


import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;

/**
 * Helper calss for animations
 */
public final  class AnimHelper {

    public final static int SHORT_DURATION=200;
    public final static int MED_DURATION=275;
    public final static int LONG_DURATION=325;
    public final static int VERY_LONG_DURATION=800;
    public final static int EXTRA_LONG_DURATION=1500;


    public static final String ROTATION = "rotation";
    public static final String SCALE_X = "scaleX";
    public static final String SCALE_Y = "scaleY";
    public static final String TRANSLATION_X = "translationX";
    public static final String TRANSLATION_Y = "translationY";




    public static PropertyValuesHolder rotation(float... values) {
        return PropertyValuesHolder.ofFloat(ROTATION, values);
    }

    public static PropertyValuesHolder translationX(float... values) {
        return PropertyValuesHolder.ofFloat(TRANSLATION_X, values);
    }

    public static PropertyValuesHolder translationY(float... values) {
        return PropertyValuesHolder.ofFloat(TRANSLATION_Y, values);
    }

    public static PropertyValuesHolder scaleX(float... values) {
        return PropertyValuesHolder.ofFloat(SCALE_X, values);
    }

    public static PropertyValuesHolder scaleY(float... values) {
        return PropertyValuesHolder.ofFloat(SCALE_Y, values);
    }
    public static void scaleView(final View v, float startScale, float endScale) {
        if(v==null)return;
        Animation anim = new ScaleAnimation(
                1f, 1f, // Start and end values for the X axis scaling
                startScale, endScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                v.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        v.startAnimation(anim);
    }
    /**
     * make scale out and in X&Y axis effect with overshoot interpolator
     * @param v the view to animate
     * @param duration duration in ms
     * @param scaleFactor the percent of scale if you want to scale view to 130% then pass 0.3f
     */
    public static void scaleInOut(@NonNull View v, int duration,float scaleFactor){
        if(duration<=0)return;
        switch (duration){
            //must have on of these durations for consistency pov :]
            case SHORT_DURATION:
            case MED_DURATION:
            case LONG_DURATION:
                break;
            default:
                duration=MED_DURATION;
        }
        ObjectAnimator scaleAnimX = ObjectAnimator.ofFloat(v, "scaleX", 1.0f,1.0f+ scaleFactor);
        scaleAnimX.setDuration(duration);
        scaleAnimX.setRepeatCount(1);
        scaleAnimX.setInterpolator(new OvershootInterpolator());
        scaleAnimX.setRepeatMode(ValueAnimator.REVERSE);
        ObjectAnimator scaleAnimY = ObjectAnimator.ofFloat(v, "scaleY", 1.0f,1.0f+ scaleFactor);
        scaleAnimY.setDuration(duration);
        scaleAnimY.setRepeatCount(1);
        scaleAnimY.setInterpolator(new OvershootInterpolator());
        scaleAnimY.setRepeatMode(ValueAnimator.REVERSE);
        scaleAnimX.start();
        scaleAnimY.start();
    }

    /**
     * make scale out and in X&Y axis effect with overshoot interpolator
     * @param v the view to animate
     * @param duration duration in ms
     */
    public static void fadeIn(@NonNull View v, int duration){
        if(duration<=0)return;
        switch (duration){
            //must have on of these durations for consistency pov :]
            case SHORT_DURATION:
            case MED_DURATION:
            case VERY_LONG_DURATION:
            case LONG_DURATION:
                break;
            default:
                duration=MED_DURATION;
        }
        v.setVisibility(View.INVISIBLE);

        ObjectAnimator scaleAnimX = ObjectAnimator.ofFloat(v, View.ALPHA, 0.0f,1.0f);
        scaleAnimX.setDuration(VERY_LONG_DURATION);
        scaleAnimX.setRepeatCount(0);
        scaleAnimX.setInterpolator(new OvershootInterpolator());
        v.setVisibility(View.VISIBLE);

        scaleAnimX.setRepeatMode(ValueAnimator.REVERSE);

        scaleAnimX.start();
    }
    public static void SlideInFromRighToLeft(@NonNull final View v, int duration){
        if(duration<=0)return;
        switch (duration){
            //must have on of these durations for consistency pov :]
            case SHORT_DURATION:
            case MED_DURATION:
            case VERY_LONG_DURATION:
            case LONG_DURATION:
                break;
            default:
                duration=MED_DURATION;
        }
        final float x= v.getTranslationX();
        ObjectAnimator scaleAnimX =     ObjectAnimator.ofFloat(v, View.TRANSLATION_X, v.getMeasuredWidth()/3, 0);
        scaleAnimX.setDuration(duration);
        scaleAnimX.setRepeatCount(0);
        scaleAnimX.setInterpolator(new OvershootInterpolator());

        scaleAnimX.setRepeatMode(ValueAnimator.REVERSE);


        scaleAnimX.start();
    }

    public static void SlideInBottom(@NonNull final View v, int duration){
        if(duration<=0)return;
        switch (duration){
            //must have on of these durations for consistency pov :]
            case SHORT_DURATION:
            case MED_DURATION:
            case VERY_LONG_DURATION:
            case LONG_DURATION:
                break;
            default:
                duration=MED_DURATION;
        }
        final float x= v.getTranslationX();
        ObjectAnimator scaleAnimX =     ObjectAnimator.ofFloat(v, View.TRANSLATION_Y, v.getMeasuredHeight()/8, 0);
        scaleAnimX.setDuration(duration);
        scaleAnimX.setRepeatCount(0);
        scaleAnimX.setInterpolator(new OvershootInterpolator());

        scaleAnimX.setRepeatMode(ValueAnimator.REVERSE);


        scaleAnimX.start();
    }

    public static void SlideOutBottom(final View v, int duration) {
        if(duration<=0)return;
        switch (duration){
            //must have on of these durations for consistency pov :]
            case SHORT_DURATION:
            case MED_DURATION:
            case VERY_LONG_DURATION:
            case LONG_DURATION:
            case EXTRA_LONG_DURATION:
                break;
            default:
                duration=MED_DURATION;
        }
        final float x= v.getTranslationX();
        ObjectAnimator scaleAnimX =     ObjectAnimator.ofFloat(v, View.TRANSLATION_Y,0,v.getMeasuredHeight()/8);
        scaleAnimX.setDuration(duration);
        scaleAnimX.setRepeatCount(0);
        scaleAnimX.setInterpolator(new OvershootInterpolator());

        scaleAnimX.setRepeatMode(ValueAnimator.REVERSE);

        v.postDelayed(new Runnable() {
            @Override
            public void run() {
                v.setVisibility(View.INVISIBLE);

            }
        },duration);
        scaleAnimX.start();
    }

    public static void wiggleEndlessly(View itemView) {
        itemView.clearAnimation();
        ObjectAnimator objectAnimator=  ObjectAnimator
                .ofFloat(itemView, "translationX", 0, 25, -25, 25, -25,15, -15, 6, -6, 0);

        objectAnimator .setDuration(VERY_LONG_DURATION);

        ObjectAnimator skewAnim = ObjectAnimator.ofFloat(itemView, View.ROTATION, -6f,6f);
        skewAnim.setInterpolator(new DecelerateInterpolator());
        objectAnimator .setRepeatMode(ValueAnimator.REVERSE);
        skewAnim .setRepeatMode(ValueAnimator.REVERSE);
        skewAnim .setDuration(120);
        objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
        skewAnim.setRepeatCount(50);
        skewAnim.start();


        //  objectAnimator.start();
        //nimationUtils.loadAnimation()
    }


}
