package com.example.amir.angular;

/**
 * Created by Amir on 1/23/2017.
 */
public class PermaRoundFragmentDelegate {
    public RoundItem getRoundItem() {
        return roundItem;
    }

    private RoundItem roundItem;

    public PermaRoundFragmentDelegate(RoundItem roundItem) {
        this.roundItem = roundItem;
    }
}
