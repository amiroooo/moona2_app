package com.example.amir.angular;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.MainThread;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Amir on 12/31/2016.
 */

public class RoundItem extends RecyclerItem implements Parcelable {

    private String id;
    private String group;
    private Integer progress;
    private Integer round_type;
    private Integer round_time;
    private Integer round_order;
    private String refereeName;
    private Long roundTimeLeft;

    public RoundItem() {

    }

    public RoundItem(String id, String group, Integer progress, Integer round_type, Integer round_time, Integer round_order) {
        set_id(id);
        this.group = group;
        this.progress = progress;
        this.round_type = round_type;
        this.round_time = round_time;
        this.round_order = round_order;
    }

    public String getRefereeName() {
        return refereeName;
    }

    public void setRefereeName(String refereeName) {
        this.refereeName = refereeName;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public Integer getRound_type() {
        return round_type;
    }

    public void setRound_type(Integer round_type) {
        this.round_type = round_type;
    }

    public Integer getRound_time() {
        return round_time;
    }

    public void setRound_time(Integer round_time) {
        this.round_time = round_time;
    }

    public Integer getRound_order() {
        if (round_type == 1){
            return 10;
        }
        return round_order;
    }

    public void setRound_order(Integer round_order) {
        this.round_order = round_order;
    }

    protected RoundItem(Parcel in) {
        id = in.readString();
        group = in.readString();
        if (in.readByte() == 0) {
            progress = null;
        } else {
            progress = in.readInt();
        }
        if (in.readByte() == 0) {
            round_type = null;
        } else {
            round_type = in.readInt();
        }
        if (in.readByte() == 0) {
            round_time = null;
        } else {
            round_time = in.readInt();
        }

        if (in.readByte() == 0) {
            round_order = null;
        } else {
            round_order = in.readInt();
        }
        refereeName = in.readString();
        roundTimeLeft = in.readLong();
    }

    public static final Creator<RoundItem> CREATOR = new Creator<RoundItem>() {
        @Override
        public RoundItem createFromParcel(Parcel in) {
            return new RoundItem(in);
        }

        @Override
        public RoundItem[] newArray(int size) {
            return new RoundItem[size];
        }
    };

    public void updateByJson(JSONObject updatedValuesJson) {
        if (updatedValuesJson.has("round_order")){
            try {
                setRound_order(updatedValuesJson.getInt("round_order"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (updatedValuesJson.has("progress")){
            try {
                setProgress(updatedValuesJson.getInt("progress"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (updatedValuesJson.has("referee_name")){
            try {
                setRefereeName(updatedValuesJson.getString("referee_name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (updatedValuesJson.has("pause_left")){
            try {
                setRoundTimeLeft(updatedValuesJson.getLong("pause_left"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(group);
        if (progress == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(progress);
        }
        if (round_type == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(round_type);
        }
        if (round_time == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(round_time);
        }
        if (round_order == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(round_order);
        }
        if (roundTimeLeft == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(roundTimeLeft);
        }




    }


    public Long getRoundTimeLeft() {
        return roundTimeLeft;
    }

    public void setRoundTimeLeft(Long roundTimeLeft) {
        this.roundTimeLeft = roundTimeLeft;
    }

    @Override
    String getOfficialName() {
        return "RoundItem";
    }

    public String getGameTypeName() {
        if (getRound_type() == 0){
            return "Field Game";
        }else if (getRound_type() == 1){
            return "Community Game";
        }
        return "Unknown Game";
    }

    public static ArrayList<RoundPartItem> getRoundPartsForRoundId(String roundId){
        ArrayList<RecyclerItem> items = new ArrayList<>();
        ArrayList<RoundPartItem> result = new ArrayList<>();
        items.addAll(Config.roundPartsMap.values());
        for (int i = 0; i < items.size(); i++) {
            RoundPartItem element = (RoundPartItem)items.get(i);
            if (element.getRound_id().equals(roundId)) {
                result.add(element);
            }
        }
        return result;
    }


    public String calculatePoints() {


            String group_id = getGroup();
            int roundOrder = getRound_order();
            ArrayList<RoundPartItem> chosenRoundPartsList = getRoundPartsForRoundId(get_id());

            ArrayList<RoundPartItem> game1List = new ArrayList<>();
            ArrayList<RoundPartItem> game2List = new ArrayList<>();
            ArrayList<RoundPartItem> game3List = new ArrayList<>();
            ArrayList<RoundPartItem> communityList = new ArrayList<>();

            for (int i = 0; i < chosenRoundPartsList.size(); i++) {
                RoundPartItem roundPart = chosenRoundPartsList.get(i);
                switch (roundPart.getPartDataType()) {
                    case "game1": {
                        game1List.add(roundPart);
                        break;
                    }
                    case "game2": {
                        game2List.add(roundPart);
                        break;
                    }
                    case "game3": {
                        game3List.add(roundPart);
                        break;
                    }
                    case "community_score": {
                        communityList.add(roundPart);
                        break;
                    }

                }
            }


            Integer scoreHelper1 = 0;
            Integer counterHelper1 = 0;
            Integer game1Score = 0;
            for (RoundPartItem element : game1List) {
                if (element.getValue().has("entered")) {
                    try {
                        if (element.getValue().getInt("entered") > scoreHelper1) {
                            scoreHelper1 = element.getValue().getInt("entered");
                        }
                        counterHelper1 += 1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (counterHelper1 > 1) {
                game1Score = scoreHelper1 * 30;
            } else if (counterHelper1 == 1) {
                game1Score = scoreHelper1 * 20;
            }


            Double scoreHelper2 = 0.0;
            Integer counterHelper2 = 0;
            Double game2Score = 0.0;

            for (RoundPartItem element : game2List) {

                double score2test = RecyclerAdapter.getGame2RoundPartScoreRatio(element.getValue());
                if (score2test > scoreHelper2) {
                    scoreHelper2 = score2test;
                }
                counterHelper2 += 1;
            }
            if (counterHelper2 > 1) {
                double scoreRatio = scoreHelper2 * 210;
                if (scoreRatio > 210) {
                    scoreRatio = 210;
                }
                game2Score = scoreRatio;
            } else if (counterHelper2 == 1) {
                double scoreRatio = scoreHelper2 * 140;
                if (scoreRatio > 140) {
                    scoreRatio = 140;
                }
                game2Score = scoreRatio;
            }






            Double scoreHelper3 = 0.0;
            Integer counterHelper3 = 0;
            Double game3Score = 0.0;


            scoreHelper3 = RecyclerAdapter.getGroupPlaceInVoltageDevision(group_id,roundOrder);
            for (RoundPartItem element : game3List) {
                if (element.getValue().has("volts")) {
                    counterHelper3 += 1;
                }
            }
            if (counterHelper3 > 0) {
                game3Score = scoreHelper3 * 140;
                if (counterHelper3 == 2) {
                    game3Score = scoreHelper3 * 210;
                }
            }



        Integer scoreHelper4 = 0;
        Integer counterHelper4 = 0;
        Double game4Score = 0.0;
        for (RoundPartItem element : communityList) {
            if (element.getValue().has("score")) {
                try {
                    game4Score = (390.0/100) * element.getValue().getInt("score");
                    counterHelper4 += 1;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (element.getValue().has("milestone_score")) {
                try {
                    game4Score += (40.0/100) * element.getValue().getInt("milestone_score");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        Double res = Math.ceil(game3Score + game1Score + game2Score + game4Score);
        return res.toString();
    }






}
