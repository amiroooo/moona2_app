package com.example.amir.angular;

/**
 * Created by Amir on 1/16/2017.
 */
public interface SettingsDelegate {
    void performReset();
}
