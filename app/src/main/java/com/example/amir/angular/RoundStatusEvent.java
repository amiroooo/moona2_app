package com.example.amir.angular;

/**
 * Created by Amir on 1/15/2017.
 */
public class RoundStatusEvent {
    private int obs1tick;
    private int obs2tick;
    private int obs3tick;

    public int getObs4tick() {
        return obs4tick;
    }

    public void setObs4tick(int obs4tick) {
        this.obs4tick = obs4tick;
    }

    public int getObs3tick() {
        return obs3tick;
    }

    public void setObs3tick(int obs3tick) {
        this.obs3tick = obs3tick;
    }

    private int obs4tick;

    public int getObs2tick() {
        return obs2tick;
    }

    public void setObs2tick(int obs2tick) {
        this.obs2tick = obs2tick;
    }

    public int getObs1tick() {
        return obs1tick;
    }

    public void setObs1tick(int obs1tick) {
        this.obs1tick = obs1tick;
    }



    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    int status;
    String _id;

    public Integer getCounterEnds() {
        return counterEnds;
    }

    public void setCounterEnds(Integer counterEnds) {
        this.counterEnds = counterEnds;
    }

    Integer counterEnds;

    public int getObsType() {
        return obsType;
    }

    public void setObsType(int obsType) {
        this.obsType = obsType;
    }

    int obsType;

}
