package com.example.amir.angular;

/**
 * Created by Amir on 1/8/2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.ArrayList;


/**
 * Created by Amir on 11/19/2016.
 */

public class CreateNewObstacle extends BottomSheetDialog implements View.OnClickListener{


    private EditText nameEditor;
    private EditText descriptionEditor;
    private Spinner ownerEditor;
    private EditText  maxEditor;
    private EditText  tickEditor;

    private Object context;

    public CreateNewObstacle(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = LayoutInflater.from(getContext()).inflate(R.layout.new_obstacle_sheet,null);
        setContentView(root);

        nameEditor = (EditText)root.findViewById(R.id.edit_name);
        descriptionEditor = (EditText)root.findViewById(R.id.edit_describtion);
        ownerEditor = (Spinner)root.findViewById(R.id.edit_owner);
        maxEditor = (EditText)root.findViewById(R.id.edit_max_points);
        tickEditor = (EditText)root.findViewById(R.id.edit_tick);
        //ownerEditor.setAdapter( getAdapterForSpinner(Config.groupsList));
        Button confirmButton = (Button) root.findViewById(R.id.button_confirm);
        Button cancelButton = (Button) root.findViewById(R.id.button_cancel);

        confirmButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        setCancelable(false);
    }

    private ArrayAdapter<String> getAdapterForSpinner(ArrayList<RecyclerItem> recyclerItems){
        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("none");
        if (recyclerItems != null) {
            for (RecyclerItem recyclerItem : recyclerItems) {
                String testString = recyclerItem.getOfficialName();
                if (testString != null) {
                    stringList.add(recyclerItem.getOfficialName());
                }
            }
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, stringList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        return spinnerArrayAdapter;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cancel();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_confirm){
            String tempId =  nameEditor.getText().toString();
            if (tempId.length() > 0){
                cancel();
            }
            JSONArray params = new JSONArray();
            params.put(nameEditor.getText().toString());
            params.put(descriptionEditor.getText().toString());
            params.put(ownerEditor.getSelectedItem().toString());
            params.put(maxEditor.getText().toString());
            params.put(tickEditor.getText().toString());

            JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.ADD_OBSTACLE,params,new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    if (context != null) {
                    //((RefreshFromString)context).onStringDataRecieved(response,2);
                    context = null;
                    cancel();}
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                }
            });
            MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "add_new_obstacle");

        }

        if (view.getId() == R.id.button_cancel){
            cancel();
        }
    }
}
